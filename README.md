# Elks

Elks is a wrapper around the 46elks API ([docs](http://www.46elks.com/docs/)).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'elks'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install elks

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( https://bitbucket.org/lilycode/elks/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
