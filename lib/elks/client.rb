require 'httparty'

module Elks
  class Client
    include HTTParty
    base_uri "https://api.46elks.com/a1"

    # Basic constructor
    # 
    def initialize(args)
      @auth = {username: args[:user], password: args[:password]}
    end

    def allocate_number(args={country: 'se', sms_url: nil, voice_start: nil})
      query = {country: args[:country]}
      query[:sms_url] = args[:sms_url] unless args[:sms_url].nil?
      query[:voice_start] = args[:voice_start] unless args[:voice_start].nil?
      
      options = {query: query}
      options.merge!({basic_auth: @auth})

      request = self.class.post "/Numbers", options
      puts request.body.inspect
      request.body
    end
    
    def deallocate_number(id=nil)
      modify_number(id, {active: "no"})
    end
    
    def modify_number(id=nil, args={})
      unless id.nil?
        options = {query: args}
        options.merge!({basic_auth: @auth})
        request = post "/Numbers/#{id}", options
        request.body
      else
        nil
      end
    end
    
    def number(id=nil)
      unless id.nil?
        options = {basic_auth: @auth}
        request = get "/Numbers/#{id}", options
        request.body
      else
        nil
      end
    end
    
    private
    
    def check_response(response)
      if response.code == 401
        raise AuthorizationError.new response.body
      end
    end
    
  end
end
