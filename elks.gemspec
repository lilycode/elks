# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'elks/version'

Gem::Specification.new do |spec|
  spec.name          = "elks"
  spec.version       = Elks::VERSION
  spec.authors       = ["Jan Lindblom"]
  spec.email         = ["lindblom.jan@gmail.com"]
  spec.summary       = %q{Wrapper around the 46elks API.}
  spec.description   = %q{Wrapper around the 46elks API.}
  spec.homepage      = "https://bitbucket.org/lilycode/elks"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_dependency "httparty"
end
