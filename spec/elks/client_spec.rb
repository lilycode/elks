require 'elks'
require 'pp'

describe Elks::Client, "#new" do
  it "fails without proper authentication" do
    expect{ Elks::Client.new }.to raise_error(ArgumentError)
  end
  context "with proper authentication" do
    it "can allocate a number" do
      ec = instance_double("Elks::Client", allocate_number: Elks::Response::Allocation.new)
      an = ec.allocate_number
      expect(an).to_not eq(nil)
    end
  end
end